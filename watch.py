import os
import logging
import telegram
import requests
import numpy as np
import tensorflow as tf

from pytz import timezone
from datetime import datetime
from PIL import Image
from io import BytesIO

from dotenv import load_dotenv
load_dotenv()

logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.INFO)

token = os.environ.get('TELEGRAM_TOKEN')
bot = telegram.Bot(token=token)
image_size = (64, 64)
max_number_size = 5
message_offset = 0


def predict(file_paths):
    x = []
    for file_path in file_paths:
        response = requests.get(file_path)
        image = Image.open(BytesIO(response.content))
        array = np.asarray(image)
        resized_array = tf.image.resize(array, image_size) / 255
        x.append(resized_array)

    x = np.array(x)

    model = tf.keras.models.load_model('nn_model.h5')
    predictions = model.predict(x)
    lengths = predictions[0]
    labels = np.array(predictions[1:]).swapaxes(0, 1)

    msg = ""
    for i in range(len(lengths)):
        digit_pred = labels[i]
        elem_pred = [np.argmax(digit_pred[j]) for j in range(max_number_size)]
        str_digit = "".join([str(i) for i in elem_pred if i != 10])
        msg += str_digit + '\n'

    return msg


def job():
    global message_offset
    logger.info('Last update_id - %s', message_offset)
    updates = bot.get_updates(offset=message_offset + 1, timeout=30)

    if not updates:
        return

    file_paths = list()
    chat_id = None
    for u in updates:
        chat_id = u.message.chat_id

        message_offset = u.update_id

        if not u.message.photo:
            continue

        f = u.message.photo[0].get_file()
        file_paths.append(f.file_path)

    if not file_paths:
        msg = 'Photo not found'
        bot.send_message(chat_id=chat_id, text=msg)
        return

    msg = predict(file_paths)

    bot.send_message(chat_id=chat_id, text=msg)
    return {'statusCode': 200}


if __name__ == '__main__':
    while True:
        tz = timezone('Europe/Minsk')
        now = datetime.now().astimezone(tz)
        logger.info('Start job at - %s', now)

        job()

        now = datetime.now().astimezone(tz)
        logger.info('End job at - %s\n', now)
