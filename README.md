## Telegram bot for multi-digit recognition

### Installation
1. Install virtualenv: `python3 -m venv venv`
2. Activate virtualenv: `source venv/bin/activate`
1. Install requirements: `pip install -r requirements`
2. Copy and fill in `.env` file: `cp .env.example .env`
3. Run script: `python watch.py`
